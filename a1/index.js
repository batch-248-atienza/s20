console.log("Hello World!");

let number = Number(prompt("Give me a number"));
console.log("Entered number was " + number);

for(let count = number; count >= 0; count--){

	//console.log(count);

	if(count <= 50){
		console.log("The current value is at " + count + ". Terminating the loop.");
		break;
	}

	//if the value is divisible by 10, SKIP printing the number

	else if((count % 10) === 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}

	//if the value is divisible by 5, print the number

	else if((count % 5) === 0){
		console.log(count);
	};

};

let inputString = "supercalifragilisticexpialidocious";
let outputString = "";

for(let counter = 0; counter < inputString.length; counter++){
	if(inputString[counter].toLowerCase() === "a" || inputString[counter].toLowerCase() === "e" || inputString[counter].toLowerCase() === "i" || inputString[counter].toLowerCase() === "o" || inputString[counter].toLowerCase() === "u"){
		continue;
	}else{
		outputString = outputString + inputString[counter];
	};
};

console.log(outputString);